import csv

exp = open('expenses.csv', 'r')
tot = csv.writer(open('totals.csv', 'w'))
d = {}

lines = exp.readlines()
for line in lines:
    if line != lines[0]:
        lst = line.split(',')
        string = lst[1]
        money = string[1:]
        money = float(money)
        person = lst[0]
        d[person] = d.get(person, 0) + money
for x in sorted(d):
    cash = '$' + str(d[x])
    tot.writerow((x, cash))
