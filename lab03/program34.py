import csv
acc = open('accounts.csv', 'r')
exp = open('expenses.csv', 'r')
res = csv.writer(open('results.csv', 'w'))

expdict = {}
accdict = {}

expLines = exp.readlines()
for line in expLines:
    if line != expLines[0]:
        lst = line.split(',')
        string = lst[1]
        money = string[1:]
        money = float(money)
        person = lst[0]
        expdict[person] = expdict.get(person, 0) + money
accLines = acc.readlines()
for line in accLines:
    if line == accLines[0]:
        line = line.strip()
        lst = line.split(',')
        x = 'END_BALANCE'
        res.writerow((lst[0],lst[1],lst[2],lst[3], x))
    else:
        line = line.strip()
        lst = line.split(',')
        string = lst[3]
        money = string[1:]
        money = float(money)
        personExpense = expdict.get(lst[0], 0)
        endBalance = money-personExpense
        res.writerow((lst[0], lst[1], lst[2], lst[3],'$'+str(endBalance)))
        
