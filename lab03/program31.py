import urllib
import re

file1 = urllib.urlopen('http://mdp.cdm.depaul.edu/csc299/static/data/1391993.accounts.csv')
file2 = urllib.urlopen('http://mdp.cdm.depaul.edu/csc299/static/data/1391993.expenses.csv')

readfile1 = file1.read()
readfile2 = file2.read()

file1.close()
file2.close()

accounts = open('accounts.csv', 'w')
expenses = open('expenses.csv', 'w')

accounts.write(readfile1)
expenses.write(readfile2)
