from BeautifulSoup import BeautifulSoup as Soup

import simplejson
import re

importfile = open('CSC.properties.1.xml', 'r')

d = {}

soup = Soup(importfile)

tags = soup.findAll(name=re.compile('^d\:\w+'))

for data in tags:
    d[data.name[2:]] = data.string

simplejson.dump(d, open('CSC.properties.1.json', 'w'))
