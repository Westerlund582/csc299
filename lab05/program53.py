from BeautifulSoup import BeautifulSoup as Soup

import simplejson
import urllib
import re

html = urllib.urlopen("http://www.cdm.depaul.edu/odata/Courses?$orderby=CatalogNbr&$filter=EffStatus%20eq%20'A'%20and%20SubjectId%20eq'CSC'")

page = html.read()

soup = Soup(page)

ms = soup.findAll('m:properties')

ListD =[]

for mtag in ms:
    d={}
    dtags = mtag.findAll(name=re.compile('^d\:\w+'))
    for data in dtags:
        d[data.name[2:]] = data.string
    ListD.append(d)

simplejson.dump(ListD, open('CSC.json', 'w'))
