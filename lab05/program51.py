from BeautifulSoup import BeautifulSoup as Soup

import urllib
import re

html = urllib.urlopen("http://www.cdm.depaul.edu/odata/Courses?$orderby=CatalogNbr&$filter=EffStatus%20eq%20'A'%20and%20SubjectId%20eq'CSC'")

page = html.read()


soup = Soup(page)
soup1 = soup.prettify()
open('CSC.xml', 'w').write(soup1)

m = soup.findAll('m:properties')
firstM = m[0]
open('CSC.properties.1.xml', 'w').write(str(firstM))

