from BeautifulSoup import BeautifulSoup as Soup
import urllib
import csv
import re

outputfile = csv.writer(open('names.csv', 'w'))

url = urllib.urlopen("http://www.superherodb.com/characters/")

soup = Soup(url)

links = soup.findAll('ul', attrs = {'class':'char-ul'})

for link in links:
    lis = link.findAll('li')
    for li in lis:
        x = li.find('a').text
        r = re.compile('^([^\d]*?)\d*$')
        outputfile.writerow([r.match(x).group(1)])
