from BeautifulSoup import BeautifulSoup as Soup
import urllib
import csv
import mechanize
import re

outputfile = csv.writer(open('superheroes.csv', 'w'))

url = urllib.urlopen("http://www.superherodb.com/characters/")

soup = Soup(url)
cBlocks = soup.findAll('div', attrs = {'class':'cblock'})
anchors =cBlocks[1].findAll('a')
HeroLinks = []
for a in anchors[1:]:
    HeroLinks.append('http://www.superherodb.com/characters/'+ a['href'])

header = 'NAME', ' INTELLIGENCE', ' STRENGTH', ' SPEED', ' DURABILITY', ' POWER', ' COMBAT'
outputfile.writerow(header)
for Hlink in HeroLinks:    
    heroUrl = urllib.urlopen(Hlink)
    mylist = []
    soup = Soup(heroUrl)
    NameClass = soup.find('div', attrs = {'class':'cblock titlehome'})
    Name = NameClass.find('h1').text
    mylist.append(Name)

    content = soup.find('div',{'class':'content'})
    stats = content.findAll('div',{'class':re.compile('.*gridbarvalue.*')})
    for stat in stats:
        mylist.append(int(stat.text))
        print stat.text
        print 'YOU ARE IN STATS'
    outputfile.writerow(mylist)
