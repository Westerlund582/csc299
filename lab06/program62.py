from BeautifulSoup import BeautifulSoup as Soup
import urllib
import csv

outputfile = csv.writer(open('superman.csv', 'w'))

url = urllib.urlopen("http://www.superherodb.com/superman/10-791/")

soup = Soup(url)

mylist = []

NameClass = soup.find('div', attrs = {'class':'cblock titlehome'})
Name = NameClass.find('h1').text
mylist.append(Name)

cBlock = soup.find('div', attrs = {'class':'cblock'})
links = cBlock.findAll('div', attrs = {'class':'gridbarholder'})

header = 'NAME', ' INTELLIGENCE', ' STRENGTH', ' SPEED', ' DURABILITY', ' POWER', ' COMBAT'

for link in links:
    reds = link.findAll('div', attrs = {'class':'gridbarvalue color_red'}) 
    for red in reds:
        mylist.append(red.text)
    blues = link.findAll('div', attrs = {'class':'gridbarvalue color_blue'})
    for blue in blues:
        mylist.append(blue.text)    

outputfile.writerow(header)
outputfile.writerow(mylist)
